'use strict';

var gulp = require("gulp"),
    browserSync = require('browser-sync').create(),
    sass = require("gulp-sass"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename"),
    cleanCSS = require('gulp-clean-css'),
    rigger = require('gulp-rigger'),
    autoprefixer = require('gulp-autoprefixer');

var
    compress  = false,
    production = false;

var settings = {
    htmlSrc: "./src/html",
    start: "./www",
    sass: {
        path: "www/style/scss",
        start: "www/style/scss/index.scss"
    },
    css: {
        path: "www/style/css",
        production: "www/style/css",
        name: "style",
        suffix:  ".min"
    },
    js: {
        path: "www/script",
        start: "www/script/developer.js",
        production: "www/script",
        name: "production",
        suffix: ".min"
    }
};
gulp.task('start-server', ['sass', 'js', 'html'], function() {

    browserSync.init({
        server: settings.start,
        port: 4000
    });
    gulp
        .watch(
            [
                settings.htmlSrc + "/*.html",
                settings.htmlSrc + "/**/*.html"
            ], ["html"]
        );
    gulp
        .watch(
            [
                settings.sass.path + "/*.scss",
                settings.sass.path + "/**/*.scss"
            ], ["sass"]
        );
    gulp
        .watch(
            [
                settings.js.path + "/*.js",
                settings.js.path + "/**/*.js"
            ], ["js"]
        );
});

gulp.task("sass", function(){
    var file = gulp
        .src(settings.sass.start)
        .pipe(
            sass().on('error', sass.logError)
        )
        .pipe(
            autoprefixer({
                browsers: ["last 15 versions", "> 1%", "ie 9"],
            })
        )
        .pipe(
            rename({
                basename: settings.css.name
            })
        )
        .pipe(gulp.dest(settings.css.path));
    if (compress){
        file
            .pipe(
                cleanCSS({
                    compatibility: 'ie8'
                })
            )
            .pipe(
                rename({
                    basename: settings.css.name,
                    suffix: settings.css.suffix
                })
            )
            .pipe(gulp.dest(settings.css.path));
    }
    if (production){
        file
            .pipe(
                rename({
                    basename: settings.css.name,
                    suffix: settings.css.suffix
                })
            )
            .pipe(
                gulp.dest(settings.css.production)
            );
    }

    if (browserSync)
        file.
            pipe(
                browserSync.stream()
        );
});

gulp.task("js", function(){
    var file = gulp
        .src(settings.js.start)
        .pipe(
            rename({
                basename: settings.js.name
            })
        )
        .pipe(gulp.dest(settings.js.path));
    if (compress){
        file
            .pipe(uglify())
            .pipe(
                rename({
                    basename: settings.js.name,
                    suffix: settings.js.suffix
                })
            )
            .pipe(gulp.dest(settings.js.path));
    }
    if (production){
        file
            .pipe(
                gulp.dest(settings.js.production)
            );
    }
    if (browserSync)
        browserSync.reload();
});

gulp.task("html", function(){
    gulp.src(settings.htmlSrc+'/*.html')
        .pipe(rigger())
        .pipe(gulp.dest(settings.start));
    if (browserSync)
        browserSync.reload();

})

gulp.task("default", ["start-server"]);

gulp.task("compress", function(){
    production = true;
    compress = true;
    gulp.run("html");
    gulp.run("sass");
    gulp.run("js");
})